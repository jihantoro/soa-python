# pep8 format
# import https://gitlab.com/jihantoro/soa-python
# need more validation and auth (if needed)
# do not use this for public-production mode
from flask import Flask, render_template, request
from flask import redirect, jsonify, make_response
import sqlite3, re

app = Flask(__name__)

# init sqlite3 connection, set check_same_thread to False
# to avoid programming error (sqlite3 must be on same thread)
conn = sqlite3.connect('./db-mhs.sqlite', check_same_thread=False)
conn.row_factory = sqlite3.Row
cur = conn.cursor()


@app.route('/')
def index():
    return render_template("home.html")


@app.route('/master_mahasiswa')
def master_mahasiswa():
    data = cur.execute("select * from mhs;").fetchall()
    return render_template("master_mahasiswa.html", datas=data)


@app.route('/api/getall')
def api_getall():
    data = cur.execute("select * from mhs;").fetchall()
    data = [dict(row) for row in data]
    return make_response(jsonify(data), 200)


@app.route('/tambah_data')
def tambah_data():
    return render_template("tambah_data.html")


@app.route('/do_tambah_data', methods=['POST'])
def do_tambah_data():
    nim = request.form.get('nim')
    nama = request.form.get('nama')
    angkatan = request.form.get('angkatan')

    # validate name only contain alphanumeric space dot
    is_valid_name = re.match('^[\w .]+$', nama) is not None

    # validate if nim and angkatan is only digit
    # :todo: validate length, etc
    if nim.isdigit() and is_valid_name and angkatan.isdigit():
        cur.execute('insert into mhs(nim,nama,angkatan) values(?,?,?)',
                    [nim, nama, angkatan])
        conn.commit()
    return redirect("/master_mahasiswa")


@app.route('/edit/<int:_id>')
def edit(_id):
    data = cur.execute('select * from mhs where _id = ?', [_id]).fetchone()
    if data != None:
        return render_template('edit.html', mhs=data)
    else:
        return redirect('/master_mahasiswa')


@app.route('/do_edit', methods=['POST'])
def do_edit():
    _id = request.form.get('id')

    data = cur.execute('select * from mhs where _id = ?', [_id])
    if data != None:
        nim = request.form.get('nim')
        nama = request.form.get('nama')
        angkatan = request.form.get('angkatan')

        # validate name only contain alphanumeric space dot
        is_valid_name = re.match('^[\w .]+$', nama) is not None

        # validate if nim and angkatan is only digit
        if nim.isdigit() and is_valid_name and angkatan.isdigit():
            cur.execute('update mhs set nim=?,nama=?,angkatan=? where _id=?',
                        [nim, nama, angkatan, _id])
            conn.commit()
    return redirect('/master_mahasiswa')


@app.route('/hapus/<int:_id>')
def hapus(_id):
    cur.execute("delete from mhs where _id=?", [_id])
    conn.commit()
    return redirect('/master_mahasiswa')


if __name__ == '__main__':
    app.run(debug=True)
